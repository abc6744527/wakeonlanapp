package com.example.test

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CardDefaults.shape
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.platform.TextToolbar
import androidx.compose.ui.platform.TextToolbarStatus
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.example.test.util.Device
import com.example.test.util.MaskVisualTransformation
import com.example.test.util.PreferenceUtil
import com.example.test.util.getBroadcastAddr
import com.example.test.util.isDeviceOnline
import com.example.test.util.toMac

@Composable
fun FAB(openDialog: MutableState<Boolean>) {
    FloatingActionButton(
        onClick = {
            openDialog.value = true
        }, modifier = Modifier.padding(vertical = 12.dp)
    ) {
        Icon(ImageVector.vectorResource(R.drawable.baseline_add_24), "Floating action button.")
    }
    if (openDialog.value) {
        NewDevice(openDialog)
    }
}

@Composable
fun NewDevice(openDialog: MutableState<Boolean>, device: Device? = null) {
    var deviceName by remember { mutableStateOf(device?.deviceName ?: "") }
    var deviceMac by remember { mutableStateOf(device?.deviceMac ?: "") }
    var deviceBrAddress by remember { mutableStateOf(getBroadcastAddr()) }
    var deviceHostIp by remember { mutableStateOf(device?.deviceIp ?: "") }

    Dialog(
        onDismissRequest = { openDialog.value = false },
        properties = DialogProperties(usePlatformDefaultWidth = false),

        ) {
        Card(
            modifier = Modifier.padding(start = 10.dp, top = 10.dp, end = 10.dp, bottom = 40.dp)
                .fillMaxSize()
        ) {
            Text(text = "Add Device", modifier = Modifier.padding(24.dp), fontSize = 18.sp)
            Column(modifier = Modifier.padding(start = 10.dp, end = 10.dp)) {
                OutlinedTextField(value = deviceName,
                    singleLine = true,
                    onValueChange = { deviceName = it },
                    label = { Text("Name") },
                    keyboardOptions = KeyboardOptions(
                        imeAction = ImeAction.Next
                    ),
                    modifier = Modifier.fillMaxWidth().padding(top = 5.dp, bottom = 5.dp)
                )
                OutlinedTextField(
                    value = deviceMac,
                    visualTransformation = MaskVisualTransformation("##:##:##:##:##:##"),
                    singleLine = true,
                    onValueChange = {
                        if (it.length <= 12) {
                            deviceMac = it
                        }
                    },
                    label = { Text("MAC Address") },
                    keyboardOptions = KeyboardOptions(
                        imeAction = ImeAction.Next
                    ),

                    modifier = Modifier.fillMaxWidth().padding(top = 5.dp, bottom = 5.dp)
                )
                OutlinedTextField(
                    value = deviceBrAddress,
                    singleLine = true,
                    onValueChange = { deviceBrAddress = it },
                    label = { Text("Broadcast Address (optional)") },
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Decimal, imeAction = ImeAction.Next
                    ),
                    modifier = Modifier.fillMaxWidth().padding(top = 5.dp, bottom = 5.dp)
                )
                OutlinedTextField(value = deviceHostIp,
                    singleLine = true,
                    onValueChange = { deviceHostIp = it },
                    label = { Text("IP or Hostname (optional)") },
                    modifier = Modifier.fillMaxWidth().padding(top = 5.dp, bottom = 5.dp)
                )
            }
            Spacer(modifier = Modifier.weight(1f))
            Row(
                modifier = Modifier.fillMaxWidth().padding(10.dp),
                horizontalArrangement = Arrangement.End
            ) {
                OutlinedButton(modifier = Modifier.padding(start = 5.dp, end = 5.dp),
                    onClick = { openDialog.value = false }) {
                    Text(text = "Cancel")
                }
                Button(modifier = Modifier.padding(start = 5.dp), onClick = {
                    PreferenceUtil.addDevice(
                        Device(
                            deviceName = deviceName,
                            deviceMac = deviceMac,
                            deviceBroadcast = deviceBrAddress,
                            deviceIp = deviceHostIp
                        )
                    )
                    openDialog.value = false
                }) {
                    Text(text = "Save")
                }
            }
        }
    }
}

@Composable
fun CardOutlined(device: Device, isNewDevice: Boolean = false, onLongPress: () -> Unit) {
    val isSelected = remember { mutableStateOf(false) }
    val isDialogShown = remember { mutableStateOf(false) }
    val isDeviceOnline = remember { mutableStateOf(false) }
    val deviceStatusColor = remember { mutableStateOf(Color.Red) }

    val hapticFeedback = LocalHapticFeedback.current
    Card(colors = CardDefaults.cardColors(
        containerColor = MaterialTheme.colorScheme.secondaryContainer,
    ),
        border = if (isSelected.value) {
            BorderStroke(2.dp, MaterialTheme.colorScheme.primary)
        } else null,
        modifier = Modifier.fillMaxWidth().padding(top = 5.dp, bottom = 5.dp).pointerInput(Unit) {
            detectTapGestures(onLongPress = {
                onLongPress()
                isSelected.value = !isSelected.value
                hapticFeedback.performHapticFeedback(HapticFeedbackType.LongPress)
            })
        }) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column {
                Text(
                    text = device.deviceName,
                    modifier = Modifier.padding(start = 16.dp, top = 10.dp, bottom = 5.dp),
                    textAlign = TextAlign.Center,
                    fontSize = 20.sp
                )
                Text(
                    text = if (isNewDevice) {
                        device.deviceIp
                    } else {
                        device.deviceMac.toMac()
                    },
                    modifier = Modifier.padding(start = 16.dp, top = 5.dp, bottom = 10.dp),
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Light
                )
            }
            Column(modifier = Modifier.padding(end = 16.dp)) {
                if (isNewDevice) {
                    Button(
                        onClick = { isDialogShown.value = true }, shape = MaterialTheme.shapes.small
                    ) {
                        Icon(
                            ImageVector.vectorResource(R.drawable.baseline_add_24), null
                        )
                    }
                } else {
                    LaunchedEffect(key1 = true) {
                        isDeviceOnline.value = isDeviceOnline(device)
                    }
                    deviceStatusColor.value = if (isDeviceOnline.value) Color.Green else Color.Red
                    Box(
                        modifier = Modifier.size(15.dp).clip(shape)
                            .border(1.dp, color = Color.Gray, shape)
                            .background(deviceStatusColor.value)
                    )
                }
                if (isDialogShown.value) {
                    NewDevice(isDialogShown, device)
                }
            }
        }
    }
}

@Composable
fun TextHeader(text: String) {
    Text(
        text = text,
        fontSize = 20.sp,
        fontWeight = FontWeight.Bold,
        modifier = Modifier.padding(top = 10.dp, bottom = 10.dp, end = 5.dp)
    )
}

@Composable
fun TextBody(text: String, modifier: Modifier = Modifier) {
    Text(text = text, fontSize = 15.sp, modifier = modifier)
}

@Preview
@Composable
fun Test() {
//    val isDialogShown = remember {
//        mutableStateOf(true)
//    }
//
//    NewDevice(openDialog = isDialogShown)
    CardOutlined(Device(deviceName = "Test", deviceMac = "000000000000"), false) { }
}

object EmptyTextToolbar : TextToolbar {
    override val status: TextToolbarStatus = TextToolbarStatus.Hidden

    override fun hide() {}

    override fun showMenu(
        rect: Rect,
        onCopyRequested: (() -> Unit)?,
        onPasteRequested: (() -> Unit)?,
        onCutRequested: (() -> Unit)?,
        onSelectAllRequested: (() -> Unit)?,
    ) {
    }
}