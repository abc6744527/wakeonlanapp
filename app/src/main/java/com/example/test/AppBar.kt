package com.example.test

import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.DrawerState
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.test.App.Companion.applicationScope
import com.example.test.ui.theme.TestTheme
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CustomAppBar(drawerState: DrawerState?, title: String) {
    val coroutineScope = rememberCoroutineScope()
    CenterAlignedTopAppBar(navigationIcon = {
        if (drawerState != null) {
            IconButton(onClick = {
                coroutineScope.launch {
                    drawerState.open()
                }
            }) {
                Icon(
                    ImageVector.vectorResource(R.drawable.baseline_menu_24),
                    contentDescription = ""
                )
            }
        }
    }, title = { Text(text = title) })
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CustomAppBarEdit(onPressBack: () -> Unit) {
    TopAppBar(navigationIcon = {
        IconButton(onClick = {
            applicationScope.launch {
                onPressBack()
            }
        }) {
            Icon(
                ImageVector.vectorResource(R.drawable.baseline_arrow_back_24),
                contentDescription = null
            )
        }
    }, title = { Text("1") }, actions = {
        IconButton(onClick = { }) {
            Icon(
                ImageVector.vectorResource(R.drawable.outline_edit_24),
                contentDescription = null
            )
        }

        IconButton(onClick = { }) {
            Icon(
                ImageVector.vectorResource(R.drawable.outline_delete_24),
                contentDescription = null
            )
        }
    })

}


@Preview
@Composable
fun PreviewCustomAppBar() {
    TestTheme {
        CustomAppBarEdit {}
//        CustomAppBar(drawerState = DrawerState(DrawerValue.Closed), title = "Title")
    }
}