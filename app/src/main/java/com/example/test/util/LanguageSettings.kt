package com.example.test.util

import androidx.appcompat.app.AppCompatDelegate
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.core.os.LocaleListCompat
import com.example.test.R
import java.util.Locale

@Composable
fun getLanguageDesc(language: Int): String {
    return stringResource(
        when (language) {
            ENGLISH -> R.string.la_en
            RUSSIAN -> R.string.la_ru
            else -> R.string.la_system
        }
    )
}

// Do not modify
private const val ENGLISH = 1
private const val RUSSIAN = 2

val LocaleLanguageCodeMap = mapOf(
    Locale("en", "US") to ENGLISH, Locale("ru", "RU") to RUSSIAN
)


@Composable
fun Locale?.toDisplayName(): String = this?.getDisplayName(this) ?: stringResource(
    id = R.string.la_system
)

fun setLanguage(locale: Locale?) {
    val localeList = locale?.let {
        LocaleListCompat.create(it)
    } ?: LocaleListCompat.getEmptyLocaleList()
    AppCompatDelegate.setApplicationLocales(localeList)
}