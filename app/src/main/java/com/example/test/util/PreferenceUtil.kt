package com.example.test.util

import android.content.Context
import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.datastore.dataStore
import com.example.test.App.Companion.applicationScope
import com.example.test.App.Companion.context
import com.example.test.R
import com.google.android.material.color.DynamicColors
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable
import java.util.Locale

val Context.dataStore by dataStore(fileName = "settings.json", serializer = SettingsSerializer)
val Context.devicesStore by dataStore(fileName = "devices.json", serializer = DevicesSerializer)

const val NOT_SPECIFIED = 0
const val SYSTEM_DEFAULT = NOT_SPECIFIED

object PreferenceUtil {
    private val Context.dataStore by dataStore(
        fileName = "settings.json",
        serializer = SettingsSerializer
    )
    private val Context.devicesStore by dataStore(
        fileName = "devices.json",
        serializer = DevicesSerializer
    )

    suspend fun getAppSettings(): AppSettings = context.dataStore.data.first()
    suspend fun getDevices(): Devices = context.devicesStore.data.first()

    fun saveLocalePreference(locale: Locale?) {
        if (Build.VERSION.SDK_INT >= 33) {
            // No op
        } else {
            applicationScope.launch {
                context.dataStore.updateData {
                    it.copy(language = LocaleLanguageCodeMap[locale] ?: SYSTEM_DEFAULT)
                }
            }
        }
    }

    val AppSettingsStateFlow: Flow<AppSettings>
        get() = context.dataStore.data.map {
                AppSettings(
                    darkTheme = DarkThemePreference(
                        darkThemeValue = getAppSettings().darkTheme.darkThemeValue
                    ), isDynamicColorEnabled = getAppSettings().isDynamicColorEnabled
                )
            }

    val DevicesStateFlow: Flow<Devices>
        get() = context.devicesStore.data.map {
                Devices(
                    devices = getDevices().devices
                )
            }

    fun modifyDarkThemePreference(darkThemeValue: Int) {
        applicationScope.launch {
            context.dataStore.updateData {
                it.copy(darkTheme = DarkThemePreference(darkThemeValue = darkThemeValue))
            }
        }
    }

    fun switchDynamicColor(enabled: Boolean) {
        applicationScope.launch {
            context.dataStore.updateData {
                it.copy(isDynamicColorEnabled = enabled)
            }
        }
    }

    fun addDevice(device: Device) {
        applicationScope.launch {
            val currentDevices = getDevices().devices
            context.devicesStore.updateData {
                it.copy(devices = currentDevices + device)
            }
        }
    }

    fun removeDevice(device: Device) {
        applicationScope.launch {
            val currentDevices = getDevices().devices.toMutableList()
            currentDevices.remove(device)
            context.devicesStore.updateData {
                it.copy(devices = currentDevices.toList())
            }
        }
    }
}

@Serializable
data class Devices(
    val devices: List<Device> = emptyList()
)

@Serializable
data class Device(
    val deviceName: String = "",
    val deviceMac: String = "",
    val deviceBroadcast: String = "",
    val deviceIp: String = ""
)

@Serializable
data class AppSettings(
    val darkTheme: DarkThemePreference = DarkThemePreference(),
    val isDynamicColorEnabled: Boolean = DynamicColors.isDynamicColorAvailable(),
    val language: Int = SYSTEM_DEFAULT
)

@Serializable
data class DarkThemePreference(val darkThemeValue: Int = FOLLOW_SYSTEM) {
    companion object {
        const val FOLLOW_SYSTEM = 1
        const val ON = 2
        const val OFF = 3
    }

    @Composable
    fun isDarkTheme(): Boolean {
        return if (darkThemeValue == FOLLOW_SYSTEM) isSystemInDarkTheme()
        else darkThemeValue == ON
    }

    @Composable
    fun getDarkThemeDesc(): String {
        return when (darkThemeValue) {
            FOLLOW_SYSTEM -> stringResource(R.string.la_system)
            ON -> stringResource(R.string.on)
            else -> stringResource(R.string.off)
        }
    }
}
