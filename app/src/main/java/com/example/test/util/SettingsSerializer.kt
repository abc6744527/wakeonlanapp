package com.example.test.util

import androidx.datastore.core.CorruptionException
import androidx.datastore.core.Serializer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import java.io.InputStream
import java.io.OutputStream

object SettingsSerializer : Serializer<AppSettings> {
    override val defaultValue: AppSettings
        get() = AppSettings()

    override suspend fun readFrom(input: InputStream): AppSettings {
        try {
            return Json.decodeFromString(
                AppSettings.serializer(), input.readBytes().decodeToString()
            )
        } catch (serialization: SerializationException) {
            throw CorruptionException("Unable to read UserPrefs", serialization)
        }
    }

    override suspend fun writeTo(t: AppSettings, output: OutputStream) {
        withContext(Dispatchers.IO) {
            output.write(Json.encodeToString(AppSettings.serializer(), t).encodeToByteArray())
        }
    }
}

object DevicesSerializer : Serializer<Devices> {
    override val defaultValue: Devices
        get() = Devices()

    override suspend fun readFrom(input: InputStream): Devices {
        try {
            return Json.decodeFromString(
                Devices.serializer(), input.readBytes().decodeToString()
            )
        } catch (serialization: SerializationException) {
            throw CorruptionException("Unable to read UserPrefs", serialization)
        }
    }

    override suspend fun writeTo(t: Devices, output: OutputStream) {
        withContext(Dispatchers.IO) {
            output.write(Json.encodeToString(Devices.serializer(), t).encodeToByteArray())
        }
    }
}