package com.example.test.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.LinkProperties
import android.util.Log
import com.example.test.App.Companion.context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.Inet4Address
import java.net.InetAddress


suspend fun startScanParallel(callback: (Device) -> Unit) = coroutineScope {
    val ipString = findCurrentDeviceIpString()
    val prefix = ipString.substring(0, ipString.lastIndexOf(".") + 1)
    val numThreads = Runtime.getRuntime().availableProcessors()

    val subRanges = splitIpRange(prefix, 254, numThreads)
    val jobs = subRanges.map { subRange ->
        async(Dispatchers.IO) {
            for (ip in subRange) {
                try {
                    val addr = InetAddress.getByName(ip)
                    if (addr.isReachable(50)) {
                        callback(
                            Device(
                                deviceName = addr.hostName,
                                deviceIp = addr.hostAddress?.toString() ?: ""
                            )
                        )
                    }
                } catch (e: Exception) {
                    Log.d("Network", "Не удалось подключиться к $ip", e)
                }
            }
        }
    }
    jobs.awaitAll()
}


fun findCurrentDeviceIpString(): String {
    val conManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val linkProperties: LinkProperties? = conManager.getLinkProperties(conManager.activeNetwork)
    return linkProperties?.linkAddresses?.find {
        it.address is Inet4Address
    }?.address?.hostAddress.toString()
}

fun getBroadcastAddr(): String {
    val ipString = findCurrentDeviceIpString()
    return ipString.substring(0, ipString.lastIndexOf(".") + 1) + "255"
}

private fun splitIpRange(prefix: String, rangeSize: Int, numSegments: Int): List<List<String>> {
    val segmentSize = rangeSize / numSegments
    val remainder = rangeSize % numSegments
    val segments = MutableList(numSegments) { mutableListOf<String>() }

    var currentIp = 1
    for (i in 0 until numSegments) {
        val segment = segments[i]
        for (j in 0 until segmentSize + if (i < remainder) 1 else 0) {
            segment.add("$prefix${currentIp++}")
        }
    }

    return segments
}

suspend fun sendMagicPacket(mac: String?, ip: String = getBroadcastAddr()) {
    if (mac == null) return
    val socket = withContext(Dispatchers.IO) {
        DatagramSocket()
    }
    try {
        withContext(Dispatchers.IO) {
            DatagramSocket().run {
                send(getMagicPacket(mac, ip))
                close()
            }
        }
    } catch (e: Exception) {
        Log.e("Network", e.toString())
    }
}

private fun getMagicPacket(mac: String, ip: String) =
    getMagicPacketBytes(mac).let { DatagramPacket(it, it.size, InetAddress.getByName(ip), 9) }

private fun getMagicPacketBytes(mac: String): ByteArray {
    val macBytes = getMacBytes(mac)
    return ByteArray(6 + 16 * macBytes.size).also { bytes ->
        // The Synchronization Stream is defined as 6 bytes of FFh
        for (i in 0 until 6) bytes[i] = 0xff.toByte()
        // The Target MAC block contains 16 duplications of the IEEE address of the target, with no breaks or interruptions
        for (destPos in 6.until(bytes.size).step(macBytes.size)) System.arraycopy(
            macBytes,
            0,
            bytes,
            destPos,
            macBytes.size
        )
    }
}

private fun getMacBytes(mac: String): ByteArray {
    val hex: List<String> = mac.split(":", "-")
    if (hex.size != 6) throw IllegalArgumentException("Invalid MAC address")

    return ByteArray(hex.size).also { bytes ->
        for (i in hex.indices) bytes[i] = hex[i].toInt(radix = 16).toByte()
    }
}

suspend fun isDeviceOnline(device: Device): Boolean {
    return withContext(Dispatchers.IO) {
        try {
            val addr = InetAddress.getByName(device.deviceIp)
            Log.d("Network", addr.toString())
            addr.isReachable(1500)
        } catch (e: Exception) {
            Log.d("Network", e.toString())
            false
        }
    }
}