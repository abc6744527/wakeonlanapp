package com.example.test

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.Icon
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.test.ui.screens.AboutScreen
import com.example.test.ui.screens.DevicesScreen
import com.example.test.ui.screens.HelpScreen
import com.example.test.ui.screens.NetworkScanScreen
import com.example.test.ui.screens.settings.SettingsScreen
import com.example.test.ui.screens.settings.appearance.AppearanceScreen
import com.example.test.ui.screens.settings.appearance.LanguageScreen
import com.example.test.ui.screens.settings.importExport.ImportExportScreen
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

enum class MainRoute(value: String) {
    About("about"), Devices("devices"), Help("help"), NetworkScan("network scan"), Settings("settings"), Appearance(
        "appearance"
    ),
    Languages("languages"), ImportExport("import and export")
}

data class DrawerMenu(val icon: ImageVector, val title: String, val route: String)

@Composable
fun menuItems(): Array<DrawerMenu> {
    return arrayOf(
        DrawerMenu(
            ImageVector.vectorResource(R.drawable.outline_computer_24),
            stringResource(R.string.menu_item_devices),
            MainRoute.Devices.name
        ), DrawerMenu(
            ImageVector.vectorResource(R.drawable.outline_scanner_24),
            stringResource(R.string.menu_item_networkScan),
            MainRoute.NetworkScan.name
        ), DrawerMenu(
            ImageVector.vectorResource(R.drawable.outline_help_24),
            stringResource(R.string.menu_item_help),
            MainRoute.Help.name
        ), DrawerMenu(
            ImageVector.vectorResource(R.drawable.baseline_settings_24),
            stringResource(R.string.menu_item_settings),
            MainRoute.Settings.name
        ), DrawerMenu(
            ImageVector.vectorResource(R.drawable.outline_info_24),
            stringResource(R.string.menu_item_about),
            MainRoute.About.name
        )
    )
}

@Composable
private fun DrawerContent(
    selectedItem: MutableState<DrawerMenu>, menus: Array<DrawerMenu>, onMenuClick: (String) -> Unit
) {
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Spacer(modifier = Modifier.height(26.dp))
        menus.forEach {
            NavigationDrawerItem(label = { Text(text = it.title) },
                icon = { Icon(imageVector = it.icon, contentDescription = null) },
                selected = (it == selectedItem.value),
                onClick = {
                    onMenuClick(it.route)
                    selectedItem.value = it
                })
        }
    }
}

@Composable
fun MainNavigation(
    navController: NavHostController = rememberNavController(),
    coroutineScope: CoroutineScope = rememberCoroutineScope(),
    drawerState: DrawerState = rememberDrawerState(initialValue = DrawerValue.Closed),
) {
    val drawerItemList = menuItems()
    val selectedItem = remember { mutableStateOf(drawerItemList[0]) }
    ModalNavigationDrawer(drawerState = drawerState, drawerContent = {
        ModalDrawerSheet {
            DrawerContent(selectedItem, drawerItemList) { route ->
                coroutineScope.launch {
                    drawerState.close()
                }
                navController.navigate(route)
            }
        }
    }) {
        NavHost(navController = navController, startDestination = MainRoute.Devices.name) {
            composable(MainRoute.Devices.name) {
                DevicesScreen(drawerState)
            }
            composable(MainRoute.NetworkScan.name) {
                NetworkScanScreen(drawerState)
            }
            composable(MainRoute.Help.name) {
                HelpScreen(drawerState)
            }
            composable(MainRoute.Settings.name) {
                SettingsScreen(drawerState, navController)
            }
            composable(MainRoute.About.name) {
                AboutScreen(drawerState)
            }
            composable(MainRoute.Appearance.name) {
                AppearanceScreen(navController)
            }
            composable(MainRoute.Languages.name) {
                LanguageScreen(navController)
            }
            composable(MainRoute.ImportExport.name) {
                ImportExportScreen(navController)
            }
        }
    }
}