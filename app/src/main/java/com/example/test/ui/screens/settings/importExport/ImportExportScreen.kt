package com.example.test.ui.screens.settings.importExport

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.LargeTopAppBar
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.navigation.NavHostController
import com.example.test.R
import com.example.test.ui.component.BackButton
import com.example.test.ui.component.PreferenceItem

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ImportExportScreen(navController: NavHostController) {
    val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior(
        rememberTopAppBarState(),
        canScroll = { true })

    Scaffold(modifier = Modifier.fillMaxSize().nestedScroll(scrollBehavior.nestedScrollConnection),
        topBar = {
            LargeTopAppBar(title = { Text(stringResource(R.string.import_and_export)) },
                navigationIcon = {
                    BackButton { navController.popBackStack() }
                },
                scrollBehavior = scrollBehavior
            )
        }) { paddingValues ->
        Column(
            modifier = Modifier.padding(paddingValues).verticalScroll(rememberScrollState())
        ) {
            PreferenceItem(
                title = stringResource(R.string.import_devices),
                description = stringResource(R.string.import_devices_desc),
                icon = ImageVector.vectorResource(R.drawable.outline_download_24)
            ) {}
            PreferenceItem(
                title = stringResource(R.string.export_devices),
                description = stringResource(R.string.export_devices_desc),
                icon = ImageVector.vectorResource(R.drawable.outline_file_upload_24)
            ) {}
        }
    }
}
