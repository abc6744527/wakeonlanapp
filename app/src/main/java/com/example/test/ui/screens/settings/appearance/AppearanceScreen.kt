package com.example.test.ui.screens.settings.appearance

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.LargeTopAppBar
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.navigation.NavHostController
import com.example.test.MainRoute
import com.example.test.R
import com.example.test.ui.common.LocalDarkTheme
import com.example.test.ui.common.LocalDynamicColorSwitch
import com.example.test.ui.component.BackButton
import com.example.test.ui.component.PreferenceItem
import com.example.test.ui.component.PreferenceSwitch
import com.example.test.util.DarkThemePreference.Companion.OFF
import com.example.test.util.DarkThemePreference.Companion.ON
import com.example.test.util.PreferenceUtil
import com.example.test.util.toDisplayName

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppearanceScreen(navController: NavHostController) {
    val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior(
        rememberTopAppBarState(),
        canScroll = { true })

    Scaffold(modifier = Modifier.fillMaxSize().nestedScroll(scrollBehavior.nestedScrollConnection),
        topBar = {
            LargeTopAppBar(title = { Text(stringResource(R.string.look_and_feel)) },
                navigationIcon = {
                    BackButton { navController.popBackStack() }
                },
                scrollBehavior = scrollBehavior
            )
        }) { paddingValues ->
        Column(
            modifier = Modifier.padding(paddingValues).verticalScroll(rememberScrollState())
        ) {
            val isDynamicColor = LocalDynamicColorSwitch.current
            PreferenceSwitch(
                title = stringResource(R.string.dynamic_color),
                description = stringResource(R.string.dynamic_color_desc),
                icon = ImageVector.vectorResource(R.drawable.outline_color_lens_24),
                isChecked = LocalDynamicColorSwitch.current,
                enabled = true
            ) {
                PreferenceUtil.switchDynamicColor(!isDynamicColor)
            }
            val isDarkTheme = LocalDarkTheme.current.isDarkTheme()
            PreferenceSwitch(
                title = stringResource(R.string.dark_theme),
                description = LocalDarkTheme.current.getDarkThemeDesc(),
                icon = if (isDarkTheme) ImageVector.vectorResource(R.drawable.outline_dark_mode_24) else ImageVector.vectorResource(
                    R.drawable.outline_light_mode_24
                ),
                isChecked = isDarkTheme,
                enabled = true
            ) {
                PreferenceUtil.modifyDarkThemePreference(if (isDarkTheme) OFF else ON)
            }
            PreferenceItem(
                title = stringResource(R.string.language),
                description = java.util.Locale.getDefault().toDisplayName(),
                icon = ImageVector.vectorResource(R.drawable.baseline_language_24)
            ) {
                navController.navigate(MainRoute.Languages.name)
            }
        }
    }
}