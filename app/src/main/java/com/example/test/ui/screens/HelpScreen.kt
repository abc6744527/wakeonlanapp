package com.example.test.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.DrawerState
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.test.CustomAppBar
import com.example.test.R
import com.example.test.TextBody
import com.example.test.TextHeader

@Composable
fun HelpScreen(drawerState: DrawerState) {
    val scrollState = rememberScrollState(0)
    Scaffold(topBar = {
        CustomAppBar(
            drawerState = drawerState, title = stringResource(R.string.menu_item_help)
        )
    }) { paddingValues ->
        Column(
            modifier = Modifier.padding(paddingValues).fillMaxSize().padding(start = 10.dp)
                .verticalScroll(scrollState),
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Top
        ) {
            TextHeader(text = stringResource(R.string.help_what_is_wol_header))
            TextBody(text = stringResource(R.string.help_what_is_wol_body))
            TextHeader(text = stringResource(R.string.help_how_wake_up_pc_header))
            TextBody(text = stringResource(R.string.help_how_wake_up_pc_body))
            TextBody(
                text = stringResource(R.string.help_how_wake_up_pc_list),
                modifier = Modifier.padding(start = 10.dp, top = 10.dp, bottom = 10.dp)
            )
            TextBody(text = stringResource(R.string.help_how_wake_up_pc_body2))
        }
    }
}