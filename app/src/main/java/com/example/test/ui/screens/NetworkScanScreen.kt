package com.example.test.ui.screens

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.pulltorefresh.PullToRefreshContainer
import androidx.compose.material3.pulltorefresh.rememberPullToRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.test.CardOutlined
import com.example.test.CustomAppBar
import com.example.test.R
import com.example.test.util.Device
import com.example.test.util.startScanParallel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun NetworkScanScreen(drawerState: DrawerState) {
    val state = rememberPullToRefreshState(positionalThreshold = 60.dp)
    val deviceScanned = remember { mutableStateListOf<Device>() }

    if (state.isRefreshing) {
        LaunchedEffect(key1 = true) {
            deviceScanned.clear()
            startScanParallel {
                deviceScanned.add(it)
            }
            state.endRefresh()
        }
    }

    Scaffold(
        topBar = {
            CustomAppBar(
                drawerState = drawerState, title = stringResource(R.string.menu_item_networkScan)
            )
        },
    ) { paddingValues ->
        Box(modifier = Modifier.nestedScroll(state.nestedScrollConnection).padding(paddingValues)) {
            LazyColumn(
                modifier = Modifier.fillMaxSize()
            ) {
                items(deviceScanned) { device ->
                    CardOutlined(
                        device, isNewDevice = true
                    ) {}
                }
            }
            PullToRefreshContainer(
                modifier = Modifier.align(Alignment.TopCenter), state = state
            )
        }
    }
}

@Preview
@Composable
fun PreviewScreen() {
    NetworkScanScreen(DrawerState(DrawerValue.Open))
}