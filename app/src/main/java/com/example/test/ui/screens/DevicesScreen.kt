package com.example.test.ui.screens

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.DrawerState
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SwipeToDismissBox
import androidx.compose.material3.SwipeToDismissBoxValue
import androidx.compose.material3.minimumInteractiveComponentSize
import androidx.compose.material3.rememberSwipeToDismissBoxState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import com.example.test.App.Companion.applicationScope
import com.example.test.CardOutlined
import com.example.test.CustomAppBar
import com.example.test.CustomAppBarEdit
import com.example.test.FAB
import com.example.test.R
import com.example.test.ui.common.LocalDevices
import com.example.test.util.PreferenceUtil
import com.example.test.util.sendMagicPacket
import com.example.test.util.toMac
import kotlinx.coroutines.launch

@Composable
fun DevicesScreen(drawerState: DrawerState) {
    val isDialogShown = remember { mutableStateOf(false) }
    val isEdit = remember { mutableStateOf(false) }
    val devices = LocalDevices.current.devices
    val snackbarHostState = remember { SnackbarHostState() }

    Scaffold(snackbarHost = {
        SnackbarHost(
            snackbarHostState, modifier = Modifier.padding(1.dp)
        ) {
            Snackbar(
                snackbarData = it, containerColor = MaterialTheme.colorScheme.onPrimaryContainer
            )
        }
    }, topBar = {
        if (!isEdit.value) {
            CustomAppBar(
                drawerState = drawerState, title = stringResource(R.string.menu_item_devices)
            )
        } else {
            CustomAppBarEdit {
                isEdit.value = false
            }
        }
    }, floatingActionButton = { FAB(isDialogShown) }) { paddingValues ->
        LazyColumn(
            modifier = Modifier.padding(paddingValues)
        ) {
            items(devices) {
                SwipeBox(
                    onDelete = {
                        PreferenceUtil.removeDevice(it)
                    },
                    onEdit = {
                        applicationScope.launch {
                            sendMagicPacket(it.deviceMac.toMac())
                            snackbarHostState.showSnackbar(
                                "Magic Packet sent to " + it.deviceMac.toMac(),
                                duration = SnackbarDuration.Short
                            )
                        }
                    },
                ) {
                    CardOutlined(
                        it, isNewDevice = false
                    ) {
                        isEdit.value = !isEdit.value
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun SwipeBox(
    modifier: Modifier = Modifier,
    onDelete: () -> Unit,
    onEdit: () -> Unit,
    content: @Composable () -> Unit
) {
    val swipeState = rememberSwipeToDismissBoxState()

    lateinit var icon: ImageVector
    lateinit var alignment: Alignment
    val color: Color

    when (swipeState.dismissDirection) {
        SwipeToDismissBoxValue.EndToStart -> {
            icon = ImageVector.vectorResource(R.drawable.outline_delete_24)
            alignment = Alignment.CenterEnd
            color = MaterialTheme.colorScheme.errorContainer
        }

        SwipeToDismissBoxValue.StartToEnd -> {
            icon = ImageVector.vectorResource(R.drawable.outline_power_settings_new_24)
            alignment = Alignment.CenterStart
            color =
                Color.Green.copy(alpha = 0.3f)
        }

        SwipeToDismissBoxValue.Settled -> {
            icon = ImageVector.vectorResource(R.drawable.outline_delete_24)
            alignment = Alignment.CenterEnd
            color = MaterialTheme.colorScheme.errorContainer
        }
    }

    SwipeToDismissBox(modifier = modifier.animateContentSize(),
        state = swipeState,
        backgroundContent = {
            Card(
                colors = CardDefaults.cardColors(
                    containerColor = color,
                ), modifier = Modifier.fillMaxSize().padding(top = 5.dp, bottom = 5.dp)
            ) {
                Box(
                    contentAlignment = alignment, modifier = Modifier.fillMaxSize()

                ) {
                    Icon(
                        modifier = Modifier.minimumInteractiveComponentSize(),
                        imageVector = icon,
                        contentDescription = null
                    )
                }
            }

        }) {
        content()
    }

    when (swipeState.currentValue) {
        SwipeToDismissBoxValue.EndToStart -> {
            LaunchedEffect(swipeState) {
                onDelete()
                swipeState.snapTo(SwipeToDismissBoxValue.Settled)
            }
        }

        SwipeToDismissBoxValue.StartToEnd -> {
            LaunchedEffect(swipeState) {
                onEdit()
                swipeState.snapTo(SwipeToDismissBoxValue.Settled)
            }
        }

        SwipeToDismissBoxValue.Settled -> {
        }
    }
}