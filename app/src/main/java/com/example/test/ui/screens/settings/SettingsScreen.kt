package com.example.test.ui.screens.settings

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.DrawerState
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.navigation.NavHostController
import com.example.test.CustomAppBar
import com.example.test.MainRoute
import com.example.test.R
import com.example.test.ui.component.SettingItem


@Composable
fun SettingsScreen(drawerState: DrawerState, navController: NavHostController) {

    Scaffold(modifier = Modifier.fillMaxSize(), topBar = {
        CustomAppBar(
            drawerState = drawerState, title = stringResource(R.string.menu_item_settings)
        )
    }) { paddingValues ->

        LazyColumn(
            modifier = Modifier.padding(paddingValues)
        ) {
            item {
                SettingItem(
                    title = stringResource(R.string.look_and_feel),
                    description = stringResource(R.string.display_settings),
                    icon = ImageVector.vectorResource(R.drawable.baseline_aod_24)
                ) {
                    navController.navigate(MainRoute.Appearance.name)
                }
            }
            item {
                SettingItem(
                    title = stringResource(R.string.import_and_export),
                    description = stringResource(R.string.import_and_export_desc),
                    icon = ImageVector.vectorResource(R.drawable.outline_file_upload_24)
                ) {
                    navController.navigate(MainRoute.ImportExport.name)
                }
            }
        }
    }
}
