package com.example.test.ui.screens.settings.appearance

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.LargeTopAppBar
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.test.R
import com.example.test.ui.component.BackButton
import com.example.test.ui.component.PreferenceSingleChoiceItem
import com.example.test.util.LocaleLanguageCodeMap
import com.example.test.util.PreferenceUtil
import com.example.test.util.setLanguage
import com.example.test.util.toDisplayName
import java.util.Locale

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LanguageScreen(navController: NavHostController) {
    val localeSet = LocaleLanguageCodeMap.keys
    val selectedLocale by remember { mutableStateOf(Locale.getDefault()) }

    val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior(
        rememberTopAppBarState(),
        canScroll = { true })

    Scaffold(modifier = Modifier.fillMaxSize().nestedScroll(scrollBehavior.nestedScrollConnection),
        topBar = {
            LargeTopAppBar(title = {
                Text(
                    modifier = Modifier, text = stringResource(R.string.language)
                )
            },
                navigationIcon = { BackButton { navController.popBackStack() } },
                scrollBehavior = scrollBehavior
            )
        }) {
        LazyColumn(modifier = Modifier.padding(it)) {
            item {
                PreferenceSingleChoiceItem(
                    text = stringResource(R.string.la_system),
                    selected = !localeSet.contains(selectedLocale),
                    contentPadding = PaddingValues(horizontal = 12.dp, vertical = 18.dp)
                ) {
                    PreferenceUtil.saveLocalePreference(null)
                    setLanguage(null)
                }
            }
            for (locale in localeSet) {
                item {
                    PreferenceSingleChoiceItem(
                        text = locale.toDisplayName(),
                        selected = selectedLocale == locale,
                        contentPadding = PaddingValues(horizontal = 12.dp, vertical = 18.dp)
                    ) {
                        PreferenceUtil.saveLocalePreference(locale)
                        setLanguage(locale)
                    }
                }
            }
        }
    }
}