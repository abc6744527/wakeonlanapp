package com.example.test.ui.common

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import com.example.test.util.AppSettings
import com.example.test.util.DarkThemePreference
import com.example.test.util.Devices
import com.example.test.util.PreferenceUtil

val LocalDarkTheme = compositionLocalOf { DarkThemePreference() }
val LocalDynamicColorSwitch = compositionLocalOf { false }
val LocalDevices = compositionLocalOf { Devices() }

@Composable
fun SettingsProvider(content: @Composable () -> Unit) {
    val appSettings =
        PreferenceUtil.AppSettingsStateFlow.collectAsState(initial = AppSettings()).value
    val appDevices = PreferenceUtil.DevicesStateFlow.collectAsState(initial = Devices()).value

    CompositionLocalProvider(
        LocalDarkTheme provides appSettings.darkTheme,
        LocalDynamicColorSwitch provides appSettings.isDynamicColorEnabled,
        LocalDevices provides appDevices,
        content = content
    )
}