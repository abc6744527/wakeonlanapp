package com.example.test.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.DrawerState
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import com.example.test.CustomAppBar
import com.example.test.R

@Composable
fun AboutScreen(drawerState: DrawerState) {
    Scaffold(topBar = {
        CustomAppBar(
            drawerState = drawerState,
            title = stringResource(R.string.menu_item_about)
        )
    }) { paddingValues ->
        Column(
            modifier = Modifier.padding(paddingValues).fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                imageVector = ImageVector.vectorResource(R.drawable.ic_launcher_foreground),
                contentDescription = null,
                modifier = Modifier.padding(10.dp)
            )
            Text(text = stringResource(id = R.string.app_name))
            Text(text = "Разработал студент группы УБВТ2101")
            Text(text = "Кустов А.И.")
        }
    }
}