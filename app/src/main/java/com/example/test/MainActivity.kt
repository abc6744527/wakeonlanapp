package com.example.test

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.example.test.App.Companion.context
import com.example.test.ui.common.LocalDarkTheme
import com.example.test.ui.common.LocalDynamicColorSwitch
import com.example.test.ui.common.SettingsProvider
import com.example.test.ui.theme.TestTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.hide()
        super.onCreate(savedInstanceState)
        context = this.baseContext
        setContent {
            SettingsProvider {
                TestTheme(
                    darkTheme = LocalDarkTheme.current.isDarkTheme(),
                    dynamicColor = LocalDynamicColorSwitch.current
                ) {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        MainNavigation()
                    }
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent) {
        handleShareIntent(intent)
        super.onNewIntent(intent)
    }

    private fun handleShareIntent(intent: Intent) {
        Log.d(TAG, "handleShareIntent: $intent")
    }

    companion object {
        private const val TAG = "MainActivity"
        private var sharedUrl = ""
    }
}
